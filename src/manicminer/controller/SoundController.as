/**
 * Created by katemorris on 30/11/2014.
 */
package manicminer.controller
{
import flash.media.Sound;
import flash.media.SoundChannel;

import manicminer.constant.EmbeddedSounds;

public class SoundController
{
    public static const SOUND_MATCH : String = "match";
    public static const SOUND_SWAP : String = "swap";
    public static const SOUND_TICK : String = "tick";
    public static const SOUND_WRONG : String = "wrong";
    public static const SOUND_MUSIC : String = "music";
    public static const SOUND_OUTRO : String = "outro";
    public static const SOUND_CLICK : String = "click";

    private static var _soundChannel : SoundChannel;

    public static function playMusic(musicName : String, startTime : Number = 0, loops : int = 0) : void
    {
        _soundChannel = getSoundInstance(musicName).play(startTime, loops);
    }

    public static function stopMusic() : void
    {
        _soundChannel.stop();
    }

    public static function playSound(soundName : String, startTime : Number = 0, loops : int = 0) : void
    {
        getSoundInstance(soundName).play(startTime, loops);
    }

    private static function getSoundInstance(soundName : String) : Sound
    {
        var sound : Sound;

        switch(soundName)
        {
            case SOUND_MATCH:
                sound = Sound(new EmbeddedSounds.sfxMatch());
                break;
            case SOUND_SWAP:
                sound = Sound(new EmbeddedSounds.sfxSwap());
                break;
            case SOUND_TICK:
                sound = Sound(new EmbeddedSounds.sfxTick());
                break;
            case SOUND_WRONG:
                sound = Sound(new EmbeddedSounds.sfxWrong());
                break;
            case SOUND_MUSIC:
                sound = Sound(new EmbeddedSounds.sfxMusic());
                break;
            case SOUND_CLICK:
                sound = Sound(new EmbeddedSounds.sfxClick());
                break;
            case SOUND_OUTRO:
                sound = Sound(new EmbeddedSounds.sfxOutro());
                break;
        }

        return sound;
    }
}
}
