/**
 * Created by katemorris on 28/11/2014.
 */
package manicminer.controller.command {
import flash.display.DisplayObject;

import manicminer.event.EndGameEvent;
import manicminer.view.GameView;
import manicminer.view.ScoreScreenView;

import org.robotlegs.mvcs.Command;

public class EndGameCommand extends Command
{
    [Inject]
    public var event : EndGameEvent;

    override public function execute() : void
    {
        for (var i : int = 0; i < contextView.numChildren; i++)
        {
            var child : DisplayObject = contextView.getChildAt(i);

            if (child is GameView)
            {
                mediatorMap.removeMediatorByView(child);
                contextView.removeChild(child);
                child = null;
                break;
            }
        }

        commandMap.release(this);

        contextView.addChild(new ScoreScreenView(event.getScore()));
    }
}
}
