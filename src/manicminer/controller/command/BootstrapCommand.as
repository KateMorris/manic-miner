/**
 * Created by katemorris on 28/11/2014.
 */
package manicminer.controller.command
{
import manicminer.constant.EmbeddedGraphics;
import manicminer.event.BootstrapEvent;
import manicminer.view.InstructionsScreenView;

import org.robotlegs.mvcs.Command;

public class BootstrapCommand extends Command
{
    override public function execute() : void
    {
        contextView.addChild(new EmbeddedGraphics.gfxBackground());
        contextView.addChild(new InstructionsScreenView());

        eventDispatcher.dispatchEvent(new BootstrapEvent(BootstrapEvent.BOOTSTRAP_COMPLETE));

        commandMap.release(this);
    }
}
}
