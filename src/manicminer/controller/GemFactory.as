/**
 * Created by katemorris on 29/11/2014.
 */
package manicminer.controller
{
import flash.display.DisplayObject;
import manicminer.constant.EmbeddedGraphics;
import manicminer.constant.GlobalConstants;
import manicminer.controller.component.GemComponent;

public class GemFactory
{
    public static function generateGem(col : int, row : int) : GemComponent
    {
        var gemNumber : Number = Math.floor((Math.random() * GlobalConstants.NUM_GEM_TYPES) + 1);
        var gemComp : GemComponent = new GemComponent(col, row, getGemAsset(gemNumber), gemNumber);
        gemComp.setCol(col);
        gemComp.setRow(row);

        return gemComp;
    }

    private static function getGemAsset(gemNumber : Number) : DisplayObject
    {
        var gem : DisplayObject;

        switch(gemNumber)
        {
            case GlobalConstants.GEM_TYPE_BLUE:
                gem = new EmbeddedGraphics.gfxGemBlue();
                break;
            case GlobalConstants.GEM_TYPE_GREEN:
                gem = new EmbeddedGraphics.gfxGemGreen();
                break;
            case GlobalConstants.GEM_TYPE_PURPLE:
                gem = new EmbeddedGraphics.gfxGemPurple();
                break;
            case GlobalConstants.GEM_TYPE_RED:
                gem = new EmbeddedGraphics.gfxGemRed();
                break;
            case GlobalConstants.GEM_TYPE_YELLOW:
                gem = new EmbeddedGraphics.gfxGemYellow();
                break;
        }

        return gem;
    }
}
}
