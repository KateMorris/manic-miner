/**
 * Created by katemorris on 28/11/2014.
 */
package manicminer.controller.mediator
{
import flash.events.TimerEvent;
import flash.utils.Timer;

import manicminer.controller.SoundController;

import manicminer.event.GameEvent;

import manicminer.view.TimerView;

import org.robotlegs.mvcs.Mediator;

public class TimerMediator extends Mediator
{
    [Inject]
    public var timerView : TimerView;

    private var _timer : Timer;
    private const START_TIME : Number = 60;

    public function TimerMediator()
    {

    }

    override public function onRegister() : void
    {
        timerView.setTimerText("" + START_TIME);

        _timer = new Timer(1000, START_TIME);
        _timer.addEventListener(TimerEvent.TIMER, onTick);
        _timer.addEventListener(TimerEvent.TIMER_COMPLETE, onTimeUp);
        _timer.start();
    }

    private function onTick(event : TimerEvent) : void
    {
        var timeLeft : int = START_TIME - _timer.currentCount;
        timerView.setTimerText("" + timeLeft);

        if (timeLeft <= 10)
        {
            timerView.turnTimerRed();
            SoundController.playSound(SoundController.SOUND_TICK);
        }
    }

    private function onTimeUp(event : TimerEvent) : void
    {
        eventDispatcher.dispatchEvent(new GameEvent(GameEvent.TIME_UP));
    }

    override public function onRemove() : void
    {
        _timer.removeEventListener(TimerEvent.TIMER, onTick);
        _timer.removeEventListener(TimerEvent.TIMER_COMPLETE, onTimeUp);
        _timer = null;
    }

}
}
