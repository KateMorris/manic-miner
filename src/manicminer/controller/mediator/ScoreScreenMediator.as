/**
 * Created by katemorris on 28/11/2014.
 */
package manicminer.controller.mediator
{
import flash.events.Event;
import flash.events.MouseEvent;
import manicminer.controller.SoundController;
import manicminer.event.GameEvent;
import manicminer.view.ScoreScreenView;

import org.robotlegs.mvcs.Mediator;

public class ScoreScreenMediator extends Mediator
{
    [Inject]
    public var scoreScreenView : ScoreScreenView;

    public function ScoreScreenMediator()
    {

    }

    override public function onRegister() : void
    {
        SoundController.playSound(SoundController.SOUND_OUTRO);
        scoreScreenView.playAgainButton.addEventListener(MouseEvent.CLICK, playAgain);
    }

    private function playAgain(event : Event) : void
    {
        SoundController.playSound(SoundController.SOUND_CLICK);
        removeView();
        scoreScreenView.playAgainButton.removeEventListener(MouseEvent.CLICK, playAgain);
        eventDispatcher.dispatchEvent(new GameEvent(GameEvent.START_GAME));
    }

    private function removeView() : void
    {
        scoreScreenView.removeChildren(0, scoreScreenView.numChildren - 1);
    }

}
}
