/**
 * Created by katemorris on 30/11/2014.
 */
package manicminer.controller.mediator
{
import flash.events.IEventDispatcher;
import flash.geom.Point;
import manicminer.constant.GlobalConstants;
import manicminer.controller.SoundController;
import manicminer.controller.component.GemComponent;
import manicminer.event.GemMatchEvent;
import manicminer.model.GemMatchModel;
import mx.collections.ArrayList;

public class GemMatchMediator
{
    private static var _grid : Array;

    private static const LEFT_DIR : Point = new Point(-1, 0);
    private static const RIGHT_DIR : Point = new Point(1, 0);
    private static const UP_DIR : Point = new Point(0, -1);
    private static const DOWN_DIR : Point = new Point(0, 1);

    private var _eventDispatcher : IEventDispatcher;

    private var _scoreMatches : Boolean;

    public function GemMatchMediator(eventDispatcher : IEventDispatcher)
    {
        _eventDispatcher = eventDispatcher;
    }

    public function matchesAreAvailable(_grid : Array) : Boolean
    {
        var gemMatchModel : GemMatchModel;
        var gemMatchesAvailable : Boolean;

        for (var col : int = 0; col < _grid.length; col++)
        {
            for (var row : int = _grid[col].length - 1; row >= 0; row--)
            {
                gemMatchModel = getGemMatchModel(_grid[col][row], null, GlobalConstants.NUM_GEMS_TO_MATCH - 1);

                if (gemMatchModel.firstGemHasMatches())
                {
                    var firstGemMatchesHorizontal : ArrayList = gemMatchModel.getFirstGemMatchesHorizontal();
                    var firstGemMatchesVertical : ArrayList = gemMatchModel.getFirstGemMatchesVertical();

                    gemMatchesAvailable = checkIsGemToMatchOnAxis(firstGemMatchesHorizontal, true) ||
                                          checkIsGemToMatchOnAxis(firstGemMatchesVertical, false);

                    if (gemMatchesAvailable)
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public function checkIsGemToMatchOnAxis(gemMatchesAxis : ArrayList, horizontalAxis : Boolean) : Boolean
    {

        var firstGem : GemComponent = GemComponent(gemMatchesAxis.getItemAt(0));
        var secondGem : GemComponent = GemComponent(gemMatchesAxis.getItemAt(gemMatchesAxis.length - 1));

        var firstColToCheck : int = firstGem.getCol();
        var firstRowToCheck : int = firstGem.getRow();
        var secondColToCheck : int = secondGem.getCol();
        var secondRowToCheck : int = secondGem.getRow();

        if (horizontalAxis)
        {
            firstColToCheck -= 1;
            secondColToCheck += 1;
        }
        else
        {
            firstRowToCheck += 1;
            secondRowToCheck -= 1;
        }

        var firstGemHasMatch : Boolean = isGemToMatch(firstGem, firstColToCheck, firstRowToCheck);
        var secondGemHasMatch : Boolean = isGemToMatch(secondGem, secondColToCheck, secondRowToCheck);

        return firstGemHasMatch || secondGemHasMatch;
    }

    public function isGemToMatch(gem : GemComponent, colToCheck : int, rowToCheck : int) : Boolean
    {
        var directionToAvoid : Point = new Point(gem.getCol() - colToCheck, gem.getRow() - rowToCheck);
        var directionToCheck : Point;


        for (var col : int = -1; col <= 1; col++)
        {
            for (var row : int = -1; row <= 1; row++)
            {
                // Don't check diagnols and the same gem
                if (row != col)
                {
                    if ((col * -1) != row)
                    {
                        directionToCheck = new Point(col, row);

                        if (!directionsMatch(directionToCheck, directionToAvoid))
                        {
                            if (gemsMatch(gem, gem.getCol() + directionToCheck.x, gem.getRow() + directionToCheck.y))
                            {
                                return true;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    public function checkForGemMatches(scoreMatches : Boolean, grid : Array, firstGem : GemComponent,
                                       secondGem : GemComponent = null) : void
    {
        _scoreMatches = scoreMatches;
        _grid = grid;

        if (secondGem)
        {
            if (gemIsAdjacent(firstGem, secondGem))
            {
                swapGems(firstGem, secondGem);
                handleGemMatches(firstGem, secondGem);
            }
            else
            {
                SoundController.playSound(SoundController.SOUND_WRONG);
            }
        }
        else
        {
            handleGemMatches(firstGem);
        }
    }

    private function gemIsAdjacent(firstGem : GemComponent, selectedGem : GemComponent) : Boolean
    {
        var gemIsSameRow : Boolean = selectedGem.getRow() == firstGem.getRow();
        var gemIsSameCol : Boolean = selectedGem.getCol() == firstGem.getCol();

        var gemIsHorizontal : Boolean = gemIsSameRow &&
                ((selectedGem.getCol() == firstGem.getCol() + 1) || (selectedGem.getCol() == firstGem.getCol() - 1));
        var gemIsVertical : Boolean = gemIsSameCol &&
                ((selectedGem.getRow() == firstGem.getRow() - 1) || (selectedGem.getRow() == firstGem.getRow() + 1));

        return gemIsHorizontal || gemIsVertical;
    }

    private function swapGems(firstGem : GemComponent, selectedGem : GemComponent) : void
    {
        var tempCol : int = firstGem.getCol();
        var tempRow : int = firstGem.getRow();

        firstGem.swapTo(selectedGem.getCol(), selectedGem.getRow());
        selectedGem.swapTo(tempCol, tempRow);

        _grid[selectedGem.getCol()][selectedGem.getRow()] = selectedGem;
        _grid[firstGem.getCol()][firstGem.getRow()] = firstGem;
    }

    private function handleGemMatches(firstGem : GemComponent, secondGem : GemComponent = null) : GemMatchModel
    {
        var gemMatchesModel : GemMatchModel = getGemMatchModel(firstGem, secondGem);

        if (gemMatchesModel.hasMatches())
        {
            if (_scoreMatches)
            {
                SoundController.playSound(SoundController.SOUND_MATCH);
            }

            _eventDispatcher.dispatchEvent(new GemMatchEvent(gemMatchesModel, _scoreMatches));
        }
        else
        {
            if (secondGem)
            {
                swapGems(firstGem, secondGem);
                SoundController.playSound(SoundController.SOUND_WRONG);
            }
        }

        return gemMatchesModel;
    }

    private function getGemMatchModel(firstGem : GemComponent, secondGem : GemComponent = null,
                                      numGemsToMatch : int = GlobalConstants.NUM_GEMS_TO_MATCH) : GemMatchModel
    {
        var firstGemMatchesHorizontal : ArrayList = new ArrayList();
        var firstGemMatchesVertical : ArrayList = new ArrayList();
        var secondGemMatchesHorizontal : ArrayList = new ArrayList();
        var secondGemMatchesVertical : ArrayList = new ArrayList();

        if (secondGem)
        {
            var gemSwapDirection : Point = new Point(secondGem.getCol() - firstGem.getCol(),
                            secondGem.getRow() - firstGem.getRow());
            secondGemMatchesHorizontal.addAll(getMatchedGemsOnAxis(secondGem,
                    LEFT_DIR, RIGHT_DIR, numGemsToMatch, new Point(-gemSwapDirection.x, -gemSwapDirection.y)));
            secondGemMatchesVertical.addAll(getMatchedGemsOnAxis(secondGem,
                    UP_DIR, DOWN_DIR, numGemsToMatch, new Point(-gemSwapDirection.x, -gemSwapDirection.y)));
            firstGemMatchesHorizontal.addAll(getMatchedGemsOnAxis(firstGem, LEFT_DIR, RIGHT_DIR,
                    numGemsToMatch, gemSwapDirection));
            firstGemMatchesVertical.addAll(getMatchedGemsOnAxis(firstGem, UP_DIR, DOWN_DIR,
                    numGemsToMatch,gemSwapDirection));
        }
        else
        {
            firstGemMatchesHorizontal.addAll(getMatchedGemsOnAxis(firstGem, LEFT_DIR, RIGHT_DIR, numGemsToMatch));
            firstGemMatchesVertical.addAll(getMatchedGemsOnAxis(firstGem, UP_DIR, DOWN_DIR, numGemsToMatch));
        }

        var gemMatchModel : GemMatchModel = new GemMatchModel(firstGem, secondGem,
                firstGemMatchesHorizontal, firstGemMatchesVertical,
                secondGemMatchesHorizontal, secondGemMatchesVertical);

        return gemMatchModel;
    }

    private function getMatchedGemsOnAxis(gemToCheck : GemComponent, firstAxisDir : Point, secondAxisDir : Point,
                                          numGemsToMatch: int, gemSwapDirection : Point = null) : ArrayList
    {
        var gemsOnAxis : ArrayList = new ArrayList();

        if (gemSwapDirection)
        {
            if (!directionsMatch(gemSwapDirection, firstAxisDir))
            {
                gemsOnAxis.addAll(getMatchedGemsInDirection(gemToCheck, firstAxisDir));
            }

            if (!directionsMatch(gemSwapDirection, secondAxisDir))
            {
                gemsOnAxis.addAll(getMatchedGemsInDirection(gemToCheck, secondAxisDir));
            }
        }
        else
        {
            gemsOnAxis.addAll(getMatchedGemsInDirection(gemToCheck, firstAxisDir));
            gemsOnAxis.addAll(getMatchedGemsInDirection(gemToCheck, secondAxisDir));
        }

        if (gemsOnAxis && gemsOnAxis.length >= numGemsToMatch - 1)
        {
            return gemsOnAxis;
        }

        return new ArrayList();
    }

    private function directionsMatch(firstDirection : Point, secondDirection : Point) : Boolean
    {
        return (firstDirection.x == secondDirection.x && firstDirection.y == secondDirection.y);
    }

    private function getMatchedGemsInDirection(selectedGem : GemComponent, dir : Point) : ArrayList
    {
        var matchedGems : ArrayList = new ArrayList();
        var columnToCheck : int = selectedGem.getCol() + dir.x;
        var rowToCheck : int = selectedGem.getRow() + dir.y;

        while (gemsMatch(selectedGem, columnToCheck, rowToCheck))
        {
            matchedGems.addItem(_grid[columnToCheck][rowToCheck]);

            selectedGem = _grid[columnToCheck][rowToCheck];

            columnToCheck += dir.x;
            rowToCheck += dir.y;
        }

        return matchedGems;
    }

    private function gemsMatch(selectedGem : GemComponent, columnToCheck : int, rowToCheck : int) : Boolean
    {
        if (gemIsInGrid(columnToCheck, rowToCheck))
        {
            var gemToCheck : GemComponent = _grid[columnToCheck][rowToCheck];

            if (gemToCheck != null)
            {
                return selectedGem.getType() == gemToCheck.getType();
            }
        }

        return false;
    }

    private function gemIsInGrid(columnToCheck : int, rowToCheck : int) : Boolean
    {
        return ((columnToCheck < GlobalConstants.GRID_WIDTH) && (columnToCheck >= 0)) &&
                ((rowToCheck < GlobalConstants.GRID_HEIGHT) && (rowToCheck >= 0));
    }

}
}
