/**
 * Created by katemorris on 28/11/2014.
 */
package manicminer.controller.mediator {
import flash.events.Event;
import flash.events.MouseEvent;

import manicminer.controller.SoundController;

import manicminer.event.GameEvent;

import manicminer.view.InstructionsScreenView;

import org.robotlegs.mvcs.Mediator;

public class InstructionsScreenMediator extends Mediator
{
    [Inject]
    public var instructionsScreenView : InstructionsScreenView;

    public function InstructionsScreenMediator()
    {

    }

    override public function onRegister() : void
    {
        instructionsScreenView.playButton.addEventListener(MouseEvent.CLICK, startGame);
    }

    private function startGame(event : Event) : void
    {
        SoundController.playSound(SoundController.SOUND_CLICK);
        instructionsScreenView.playButton.removeEventListener(MouseEvent.CLICK, startGame);
        eventDispatcher.dispatchEvent(new GameEvent(GameEvent.START_GAME));
    }
}
}
