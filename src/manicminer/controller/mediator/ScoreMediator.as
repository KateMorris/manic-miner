/**
 * Created by katemorris on 28/11/2014.
 */
package manicminer.controller.mediator
{
import flash.events.Event;

import manicminer.constant.GlobalConstants;

import manicminer.event.EndGameEvent;
import manicminer.event.GameEvent;
import manicminer.event.ScoreEvent;

import manicminer.view.ScoreView;

import org.robotlegs.mvcs.Mediator;

public class ScoreMediator extends Mediator
{
    [Inject]
    public var scoreView : ScoreView;

    private var _score : int;

    public function ScoreMediator()
    {

    }

    override public function onRegister() : void
    {
        _score = 0;
        scoreView.setScoreText(_score);

        eventDispatcher.addEventListener(ScoreEvent.ADD_SCORE, addScore);
        eventDispatcher.addEventListener(GameEvent.TIME_UP, onTimeUp);
    }

    private function addScore(event : ScoreEvent) : void
    {
        var scoreToAdd : int = event.getScore();

        _score += (scoreToAdd * GlobalConstants.GEM_SCORE_MULTIPLIER);

        scoreView.setScoreText(_score);
    }

    private function onTimeUp(event : Event) : void
    {
        eventDispatcher.dispatchEvent(new EndGameEvent(_score));
    }

    override public function onRemove() : void
    {
        eventDispatcher.removeEventListener(ScoreEvent.ADD_SCORE, addScore);
        eventDispatcher.removeEventListener(GameEvent.TIME_UP, onTimeUp);
    }
}
}
