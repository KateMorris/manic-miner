/**
 * Created by katemorris on 28/11/2014.
 */
package manicminer.controller.mediator
{
import flash.display.DisplayObject;
import manicminer.controller.SoundController;
import manicminer.view.GameView;
import org.robotlegs.mvcs.Mediator;

public class GameMediator extends Mediator
{
    [Inject]
    public var gameView : GameView;

    public function GameMediator()
    {

    }

    override public function onRegister() : void
    {
        SoundController.playMusic(SoundController.SOUND_MUSIC, 0, 60);
    }

    override public function onRemove() : void
    {
        for (var i : int = 0; i < gameView.numChildren; i++)
        {
            var child : DisplayObject = gameView.getChildAt(i);
            mediatorMap.removeMediatorByView(child);
            gameView.removeChild(child);
            child = null;
        }

        mediatorMap.removeMediatorByView(gameView);

        SoundController.stopMusic();
    }
}
}
