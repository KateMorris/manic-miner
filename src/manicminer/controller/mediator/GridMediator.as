/**
 * Created by katemorris on 28/11/2014.
 */
package manicminer.controller.mediator
{
import flash.display.MovieClip;
import flash.events.TimerEvent;
import flash.utils.Timer;

import manicminer.constant.EmbeddedGraphics;
import manicminer.constant.GlobalConstants;
import manicminer.constant.GlobalConstants;

import manicminer.controller.GemFactory;
import manicminer.controller.component.GemComponent;
import manicminer.event.GemMatchEvent;
import manicminer.event.GemSelectEvent;
import manicminer.event.ScoreEvent;
import manicminer.model.GemMatchModel;
import manicminer.view.GridView;

import mx.collections.ArrayList;

import org.robotlegs.mvcs.Mediator;

public class GridMediator extends Mediator
{
    [Inject]
    public var gridView : GridView;

    private var _grid : Array;

    private var _gemSelected : Boolean;
    private var _firstGem : GemComponent;

    private var _gemMatchMediator : GemMatchMediator;

    public function GridMediator()
    {

    }

    override public function onRegister() : void
    {
        _gemSelected = false;

        gridView.x = 320;

        initGrid();
        fillGrid();

        gridView.addEventListener(GemSelectEvent.GEM_SELECT, onGemSelect);
        eventDispatcher.addEventListener(GemMatchEvent.GEM_MATCH, onGemMatch);

        _gemMatchMediator = new GemMatchMediator(eventDispatcher);

        checkForMatchesAcrossGrid(false);
    }

    private function initGrid() : void
    {
        _grid = new Array(GlobalConstants.GRID_WIDTH);

        for (var i : int = 0; i < _grid.length; i++)
        {
            _grid[i] = new Array(GlobalConstants.GRID_HEIGHT);
        }
    }

    private function fillGrid() : void
    {
        for (var col : int = 0; col < _grid.length; col++)
        {
            for (var row : int = 0; row < _grid[col].length; row++)
            {
                var gem : GemComponent = GemFactory.generateGem(col, row);
                _grid[col][row] = gem;
                gridView.addChild(gem);
            }
        }
    }

    private function checkForMatchesAcrossGrid(scoreMatches : Boolean) : void
    {
        for (var col : int = 0; col < _grid.length; col++)
        {
            for (var row : int = _grid[col].length - 1; row >= 0; row --)
            {
                _gemMatchMediator.checkForGemMatches(scoreMatches, _grid, _grid[col][row]);
            }
        }

        if (!_gemMatchMediator.matchesAreAvailable(_grid))
        {
            reshuffleGrid();
        }
    }

    private function reshuffleGrid() : void
    {

    }

    private function onGemSelect(event : GemSelectEvent) : void
    {
        var selectedGem : GemComponent = event.getGem();

        if (_gemSelected)
        {
            _gemSelected = false;
            _firstGem.deselect();

            _gemMatchMediator.checkForGemMatches(true, _grid, _firstGem, selectedGem);
        }
        else
        {
            selectFirstGem(selectedGem);
        }
    }

    private function onGemMatch(event : GemMatchEvent) : void
    {
        removeAllMatchedGems(event.scoreMatches, event.gemMatchModel);
    }

    private function removeAllMatchedGems(scoreMatches : Boolean, gemMatchesModel : GemMatchModel) : void
    {
        if (gemMatchesModel.firstGemHasMatches())
        {
            explodeGem(gemMatchesModel.getFirstGem());
            removeGems(gemMatchesModel.getFirstGemMatchesHorizontal());
            removeGems(gemMatchesModel.getFirstGemMatchesVertical());
        }

        if (gemMatchesModel.secondGemHasMatches())
        {
            explodeGem(gemMatchesModel.getSecondGem());
            removeGems(gemMatchesModel.getSecondGemMatchesHorizontal());
            removeGems(gemMatchesModel.getSecondGemMatchesVertical());
        }

        if (scoreMatches)
        {
            eventDispatcher.dispatchEvent(new ScoreEvent(ScoreEvent.ADD_SCORE, gemMatchesModel.totalNumMatches()));
        }

        gemsFallDown(scoreMatches);
    }

    private function removeGems(matchedGems : ArrayList) : void
    {
        for (var i : int = 0; i < matchedGems.length; i++)
        {
            var gem : GemComponent = GemComponent(matchedGems.getItemAt(i));

            explodeGem(gem);
        }
    }

    private function explodeGem(gem : GemComponent) : void
    {
        var _explodeAnim : MovieClip = new EmbeddedGraphics.gfxExplosion();
        _explodeAnim.x = gem.x + gem.width / 2;
        _explodeAnim.y = gem.y + gem.height / 2;
        gridView.addChild(_explodeAnim);

        removeGemFromGrid(gem);

        _explodeAnim.addFrameScript(_explodeAnim.totalFrames - 1, _explodeAnim.stop);
    }

    private function removeGemFromGrid(gem : GemComponent) : void
    {
        if (gem.parent == gridView)
        {
            gridView.removeChild(_grid[gem.getCol()][gem.getRow()]);
        }

        _grid[gem.getCol()][gem.getRow()] = null;
    }

    private function gemsFallDown(scoreMatches : Boolean) : void
    {
        for (var row : int = _grid[col].length - 1; row > 0; row--)
        {
            for (var col : int = 0; col < _grid.length; col++)
            {
                if (_grid[col][row] == null)
                {
                    if (row > 0)
                    {
                        for (var rowB : int = row - 1; rowB >= 0; rowB--)
                        {
                            if (_grid[col][rowB] != null)
                            {
                                _grid[col][row] = _grid[col][rowB];
                                _grid[col][rowB] = null;
                                _grid[col][row].setRow(row);
                                rowB = -1;
                            }
                        }
                    }
                }
            }
        }

        fillEmpties(scoreMatches);
    }

    private function fillEmpties(scoreMatches : Boolean) : void
    {
        for (var col : int = 0; col < _grid.length; col++)
        {
            for (var row : int = _grid[col].length - 1; row >= 0; row--)
            {
                if (_grid[col][row] == null)
                {
                    var gem : GemComponent = GemFactory.generateGem(col, row);
                    _grid[col][row] = gem;
                    gridView.addChild(gem);
                }
            }
        }

        checkForMatchesAcrossGrid(scoreMatches);
    }

    private function selectFirstGem(selectedGem : GemComponent) : void
    {
        _firstGem = selectedGem;
        _gemSelected = true;
        selectedGem.select();
    }

    private function destroyCurrentGrid() : void
    {
        for (var col : int = 0; col < _grid.length; col++)
        {
            for (var row : int = 0; row < _grid[col].length; row++)
            {
                if (_grid[col][row] != null)
                {
                    gridView.removeChild(_grid[col][row]);
                    _grid[col][row].destroy();
                    _grid[col][row] = null;
                }
            }
        }
    }

    override public function onRemove() : void
    {
        destroyCurrentGrid();
        eventDispatcher.removeEventListener(GemMatchEvent.GEM_MATCH, onGemMatch);
    }
}
}
