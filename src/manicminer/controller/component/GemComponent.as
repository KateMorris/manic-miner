/**
 * Created by katemorris on 28/11/2014.
 */
package manicminer.controller.component
{
import com.greensock.TweenLite;
import com.greensock.easing.Back;
import com.greensock.easing.Bounce;
import com.greensock.easing.Elastic;

import flash.display.DisplayObject;
import flash.display.MovieClip;
import flash.display.Sprite;
import flash.events.MouseEvent;

import manicminer.constant.EmbeddedGraphics;
import manicminer.event.GemSelectEvent;

public class GemComponent extends Sprite
{
    private var _gemAsset : DisplayObject;
    private var _col : int;
    private var _row : int;
    private var _type : int;

    private const GEM_SPACING : Number = 10;
    private const GEM_WIDTH : Number = 35;
    private const GEM_HEIGHT : Number = 35;

    private var _selector : DisplayObject;

    private var _empty : Boolean;

    private const GEM_MASK_MARGIN : int = 100;

    private var _explodeAnim : MovieClip;

    private var _onExplosionComplete : Function;

    public function GemComponent(col : int, row : int, gemAsset : DisplayObject, type : int)
    {
        _col = col;
        _row = row;

        _gemAsset = gemAsset;


        _type = type;

        _empty = false;

        this.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
    }

    private function onMouseDown(event : MouseEvent) : void
    {
        dispatchEvent(new GemSelectEvent(this, true));
    }

    public function select() : void
    {
        _selector = new EmbeddedGraphics.gfxSelector();
        _selector.x = -3;
        _selector.y = -3;
        addChild(_selector);
    }

    public function deselect() : void
    {
        removeChild(_selector);
        _selector = null;
    }

    public function setCol(value : int) : void
    {
        _col = value;
        this.x = _col * (GEM_WIDTH + GEM_SPACING);
    }

    public function swapTo(col : int, row : int) : void
    {
        _col = col;
        _row = row;
        var newX : int = _col * (GEM_WIDTH + GEM_SPACING);
        var newY : int = GEM_MASK_MARGIN + (_row * (GEM_HEIGHT + GEM_SPACING));
        TweenLite.to(this, 0.3, {x:newX, y:newY, ease:Back.easeOut});
    }

    public function setRow(value : int) : void
    {
        _row = value;
        var newY : Number = GEM_MASK_MARGIN + (_row * (GEM_HEIGHT + GEM_SPACING));
        TweenLite.to(this, 1, {y:newY, ease:Bounce.easeOut, onUpdate:onGemRowUpdate});
    }

    private function onGemRowUpdate() : void
    {
        if (!_gemAsset)
        {
            TweenLite.killDelayedCallsTo(onGemRowUpdate);
        }
        else
        {
            if (this.y >= (GEM_MASK_MARGIN - 10) &&
                    _gemAsset.parent != this)
            {
                addChild(_gemAsset);
            }
        }
    }

    public function getCol() : int
    {
        return _col;
    }

    public function getRow() : int
    {
        return _row;
    }

    public function getType() : int
    {
        return _type;
    }

    public function get empty():Boolean
    {
        return _empty;
    }

    public function set empty(value : Boolean) : void
    {
        _empty = value;
    }

    private function removeGemAsset() : void
    {
        if (_gemAsset)
        {
            removeChild(_gemAsset);
        }
    }

    public function removeListeners() : void
    {
        TweenLite.killDelayedCallsTo(onGemRowUpdate);
        this.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
    }

    public function destroy() : void
    {
        removeListeners();

        if (_explodeAnim)
        {
            removeChild(_explodeAnim);
            _explodeAnim = null;
        }

        if (_gemAsset)
        {
            _gemAsset = null;
        }

        if (_onExplosionComplete)
        {
            _onExplosionComplete(this);
        }
    }
}
}
