/**
 * Created by katemorris on 29/11/2014.
 */
package manicminer.controller
{
import flash.display.DisplayObject;
import manicminer.constant.EmbeddedAssets;
import manicminer.constant.GlobalConstants;
import manicminer.controller.component.GemComponent;

public class GemGenerator
{
    public static function generateGem(col : int, row : int) : GemComponent
    {
        var gemNumber : Number = Math.floor((Math.random() * GlobalConstants.NUM_GEMS) + 1);
        var gemComp : GemComponent = new GemComponent(col, row, getGemAsset(gemNumber), gemNumber);
        gemComp.setCol(col);
        gemComp.setRow(row);

        return gemComp;
    }

    private static function getGemAsset(gemNumber : Number) : DisplayObject
    {
        var gem : DisplayObject;

        switch(gemNumber)
        {
            case GlobalConstants.GEM_TYPE_BLUE:
                gem = new EmbeddedAssets.GemBlue();
                break;
            case GlobalConstants.GEM_TYPE_GREEN:
                gem = new EmbeddedAssets.GemGreen();
                break;
            case GlobalConstants.GEM_TYPE_PURPLE:
                gem = new EmbeddedAssets.GemPurple();
                break;
            case GlobalConstants.GEM_TYPE_RED:
                gem = new EmbeddedAssets.GemRed();
                break;
            case GlobalConstants.GEM_TYPE_YELLOW:
                gem = new EmbeddedAssets.GemYellow();
                break;
        }

        return gem;
    }
}
}
