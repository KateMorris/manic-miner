/**
 * Created by katemorris on 28/11/2014.
 */
package manicminer.view
{
import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.text.TextField;
import flash.text.TextFormat;

import flashx.textLayout.formats.TextAlign;

import manicminer.constant.EmbeddedGraphics;

public class ScoreScreenView extends Sprite
{
    private var _scoreScreen : DisplayObject;

    private var _scoreText : TextField;
    private var _scoreTextFormat : TextFormat;
    private var _scoreTextShadowFormat : TextFormat;
    private var _scoreTextShadow : TextField;

    private var _playAgainButton : Sprite;

    public function ScoreScreenView(score : int)
    {
        _scoreScreen = new EmbeddedGraphics.gfxScoreScreen();
        addChild(_scoreScreen);

        _scoreTextFormat = new TextFormat();
        _scoreTextFormat.font = "GameFont";
        _scoreTextFormat.size = 100;
        _scoreTextFormat.color = 0xFFFFFF;
        _scoreTextFormat.align = TextAlign.CENTER;

        _scoreTextShadowFormat = new TextFormat();
        _scoreTextShadowFormat.font = _scoreTextFormat.font;
        _scoreTextShadowFormat.color = 0x000000;
        _scoreTextShadowFormat.size = _scoreTextFormat.size;
        _scoreTextShadowFormat.align = TextAlign.CENTER;

        _scoreText = new TextField();
        _scoreText.embedFonts = true;
        _scoreText.defaultTextFormat = _scoreTextFormat;
        _scoreText.x = 250;
        _scoreText.y = 250;
        _scoreText.text = "" + score;
        _scoreText.width = 250;

        _scoreTextShadow = new TextField();
        _scoreTextShadow.embedFonts = true;
        _scoreTextShadow.defaultTextFormat = _scoreTextShadowFormat;
        _scoreTextShadow.x = _scoreText.x + 3;
        _scoreTextShadow.y = _scoreText.y + 3;
        _scoreTextShadow.width = 250;
        _scoreTextShadow.text = _scoreText.text;

        addChild(_scoreTextShadow);
        addChild(_scoreText);

        _playAgainButton = new Sprite();
        _playAgainButton.addChild(new EmbeddedGraphics.gfxPlayAgainButton());
        _playAgainButton.x = 275;
        _playAgainButton.y = 410;
        addChild(_playAgainButton);
    }

    public function get playAgainButton() : DisplayObject
    {
        return _playAgainButton;
    }
}
}
