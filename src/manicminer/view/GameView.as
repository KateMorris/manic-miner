/**
 * Created by katemorris on 28/11/2014.
 */
package manicminer.view
{
import flash.display.Sprite;

import manicminer.event.ScoreEvent;

public class GameView extends Sprite
{
    private var _timerView : TimerView;
    private var _scoreView : ScoreView;
    private var _gridView : GridView;

    public function GameView()
    {
        _timerView = new TimerView();
        addChild(_timerView);

        _scoreView = new ScoreView();
        addChild(_scoreView);

        _gridView = new GridView();
        addChild(_gridView);
    }
}
}
