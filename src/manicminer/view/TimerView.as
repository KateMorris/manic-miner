/**
 * Created by katemorris on 28/11/2014.
 */
package manicminer.view
{
import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.text.TextField;
import flash.text.TextFormat;

import manicminer.constant.EmbeddedGraphics;

public class TimerView extends Sprite
{
    private var _timerText : TextField;
    private var _timerTextFormat : TextFormat;

    private var _timerTextShadow : TextField;
    private var _timerTextShadowFormat : TextFormat;

    private var _timerIcon : DisplayObject;

    public function TimerView()
    {
        _timerIcon = new EmbeddedGraphics.gfxTimer();
        _timerIcon.x = 60;
        _timerIcon.y = 50;
        addChild(_timerIcon);

        _timerTextFormat = new TextFormat();
        _timerTextFormat.font = "GameFont";
        _timerTextFormat.color = 0xFFFFFF;
        _timerTextFormat.size = 60;

        _timerTextShadowFormat = new TextFormat();
        _timerTextShadowFormat.font = _timerTextFormat.font;
        _timerTextShadowFormat.color = 0x000000;
        _timerTextShadowFormat.size = _timerTextFormat.size;

        _timerText = new TextField();
        _timerText.embedFonts = true;
        _timerText.x = _timerIcon.x + 75;
        _timerText.y = _timerIcon.y + 12;
        _timerText.text = "";
        _timerText.defaultTextFormat = _timerTextFormat;

        _timerTextShadow = new TextField();
        _timerTextShadow.embedFonts = true;
        _timerTextShadow.x = _timerText.x + 3;
        _timerTextShadow.y = _timerText.y + 3;
        _timerTextShadow.text = "";
        _timerTextShadow.defaultTextFormat = _timerTextShadowFormat;

        addChild(_timerTextShadow);
        addChild(_timerText);
    }

    public function setTimerText(newTimerText : String) : void
    {
        _timerTextShadow.text = newTimerText;
        _timerText.text = newTimerText;
    }

    public function turnTimerRed() : void
    {
        _timerText.textColor = 0xFF0000;
    }
}
}
