/**
 * Created by katemorris on 28/11/2014.
 */
package manicminer.view
{
import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.text.TextField;
import flash.text.TextFormat;

import flashx.textLayout.formats.TextAlign;

import manicminer.constant.EmbeddedGraphics;

public class ScoreView extends Sprite
{
    private var _scoreTitle : TextField;
    private var _scoreText : TextField;
    private var _scoreTextFormat : TextFormat;
    private var _scoreTitleFormat : TextFormat;
    private var _scoreTitleShadowFormat : TextFormat;
    private var _scoreTextShadowFormat : TextFormat;
    private var _scoreTextShadow : TextField;
    private var _scoreTitleShadow : TextField;

    public function ScoreView()
    {
        _scoreTextFormat = new TextFormat();
        _scoreTextFormat.font = "GameFont";
        _scoreTextFormat.size = 60;
        _scoreTextFormat.color = 0xFFFFFF;
        _scoreTextFormat.align = TextAlign.CENTER;

        _scoreTextShadowFormat = new TextFormat();
        _scoreTextShadowFormat.font = _scoreTextFormat.font;
        _scoreTextShadowFormat.color = 0x000000;
        _scoreTextShadowFormat.size = _scoreTextFormat.size;
        _scoreTextShadowFormat.align = TextAlign.CENTER;

        _scoreTitleFormat = new TextFormat();
        _scoreTitleFormat.font = "GameFont";
        _scoreTitleFormat.size = 40;
        _scoreTitleFormat.color = 0xFFFFFF;
        _scoreTitleFormat.align = TextAlign.CENTER;

        _scoreTitleShadowFormat = new TextFormat();
        _scoreTitleShadowFormat.font = _scoreTitleFormat.font;
        _scoreTitleShadowFormat.color = 0x000000;
        _scoreTitleShadowFormat.size = _scoreTitleFormat.size;
        _scoreTitleShadowFormat.align = TextAlign.CENTER;

        _scoreTitle = new TextField();
        _scoreTitle.embedFonts = true;
        _scoreTitle.defaultTextFormat = _scoreTitleFormat;
        _scoreTitle.x = 50;
        _scoreTitle.y = 130;
        _scoreTitle.text = "Score:";
        _scoreTitle.width = 170;

        _scoreTitleShadow = new TextField();
        _scoreTitleShadow.embedFonts = true;
        _scoreTitleShadow.defaultTextFormat = _scoreTitleShadowFormat;
        _scoreTitleShadow.text = _scoreTitle.text;
        _scoreTitleShadow.x = _scoreTitle.x + 3;
        _scoreTitleShadow.y = _scoreTitle.y + 3;
        _scoreTitleShadow.width = 170;

        addChild(_scoreTitleShadow);
        addChild(_scoreTitle);

        _scoreText = new TextField();
        _scoreText.embedFonts = true;
        _scoreText.defaultTextFormat = _scoreTextFormat;
        _scoreText.x = _scoreTitle.x - 15;
        _scoreText.y = _scoreTitle.y + 40;
        _scoreText.width = 200;

        _scoreTextShadow = new TextField();
        _scoreTextShadow.embedFonts = true;
        _scoreTextShadow.defaultTextFormat = _scoreTextShadowFormat;
        _scoreTextShadow.x = _scoreText.x + 3;
        _scoreTextShadow.y = _scoreText.y + 3;
        _scoreTextShadow.width = 200;

        addChild(_scoreTextShadow);
        addChild(_scoreText);
    }

    public function setScoreText(score : int) : void
    {
        _scoreTextShadow.text = "" + score;
        _scoreText.text = "" + score;

    }
}
}
