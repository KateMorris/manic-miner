/**
 * Created by katemorris on 28/11/2014.
 */
package manicminer.view
{
import flash.display.DisplayObject;
import flash.display.Sprite;
import manicminer.constant.EmbeddedGraphics;

public class InstructionsScreenView extends Sprite
{
    private var _instructionScreen : DisplayObject;
    private var _playButton : Sprite;

    public function InstructionsScreenView()
    {
        _instructionScreen = new EmbeddedGraphics.gfxInstructionScreen();
        addChild(_instructionScreen);

        _playButton = new Sprite();
        _playButton.addChild(new EmbeddedGraphics.gfxPlayButton());
        _playButton.x = 275;
        _playButton.y = 410;
        addChild(_playButton);
    }

    public function get playButton() : Sprite
    {
        return _playButton;
    }
}
}
