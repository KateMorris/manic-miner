/**
 * Created by katemorris on 28/11/2014.
 */
package manicminer
{
import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.events.Event;
import flash.text.Font;

import manicminer.constant.EmbeddedGraphics;

import manicminer.controller.command.BootstrapCommand;
import manicminer.controller.command.EndGameCommand;
import manicminer.controller.mediator.GameMediator;
import manicminer.controller.mediator.GridMediator;
import manicminer.controller.mediator.InstructionsScreenMediator;
import manicminer.controller.mediator.ScoreMediator;
import manicminer.controller.mediator.ScoreScreenMediator;
import manicminer.controller.mediator.TimerMediator;
import manicminer.event.BootstrapEvent;
import manicminer.event.EndGameEvent;
import manicminer.event.GameEvent;
import manicminer.view.GameView;
import manicminer.view.GridView;
import manicminer.view.InstructionsScreenView;
import manicminer.view.ScoreScreenView;
import manicminer.view.ScoreView;
import manicminer.view.TimerView;
import org.robotlegs.base.ContextEvent;
import org.robotlegs.mvcs.Context;

public class Context extends org.robotlegs.mvcs.Context
{
    public function Context(contextView : DisplayObjectContainer)
    {
        super(contextView);
    }

    override public function startup() : void
    {
        addEventListener(ContextEvent.STARTUP_COMPLETE, onStartUpComplete);

        mapMediators();
        mapCommands();

        super.startup();
    }

    private function onStartUpComplete(event : Event) : void
    {
        eventDispatcher.dispatchEvent(new BootstrapEvent(BootstrapEvent.BOOTSTRAP));
        eventDispatcher.addEventListener(GameEvent.START_GAME, startGame);
    }

    private function mapMediators() : void
    {
        mediatorMap.mapView(GameView, GameMediator);
        mediatorMap.mapView(GridView, GridMediator);
        mediatorMap.mapView(InstructionsScreenView, InstructionsScreenMediator);
        mediatorMap.mapView(ScoreScreenView, ScoreScreenMediator);
        mediatorMap.mapView(ScoreView, ScoreMediator);
        mediatorMap.mapView(TimerView, TimerMediator);
    }

    private function mapCommands() : void
    {
        commandMap.mapEvent(BootstrapEvent.BOOTSTRAP, BootstrapCommand);
        commandMap.mapEvent(EndGameEvent.END_GAME, EndGameCommand);
    }

    private function startGame(event : Event) : void
    {
        for (var i : int = 0; i < contextView.numChildren; i++)
        {
            var child : DisplayObject = contextView.getChildAt(i);

            if ((child is InstructionsScreenView) || (child is ScoreScreenView))
            {
                contextView.removeChild(child);
                child = null;
                break;
            }
        }

        contextView.addChild(new GameView());
    }

    override public function shutdown() : void
    {
        super.shutdown();
    }
}
}
