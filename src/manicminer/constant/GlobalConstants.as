/**
 * Created by katemorris on 29/11/2014.
 */
package manicminer.constant
{
public class GlobalConstants
{
    public static const GEM_TYPE_BLUE : int = 1;
    public static const GEM_TYPE_GREEN : int = 2;
    public static const GEM_TYPE_PURPLE : int = 3;
    public static const GEM_TYPE_RED : int = 4;
    public static const GEM_TYPE_YELLOW : int = 5;

    public static const NUM_GEM_TYPES : int = 5;

    public static const GEM_SCORE_MULTIPLIER : int = 20;

    public static const GRID_WIDTH : int = 8;
    public static const GRID_HEIGHT : int = 8;

    public static const NUM_GEMS_TO_MATCH : int = 3;
}
}
