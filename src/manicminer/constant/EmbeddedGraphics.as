/**
 * Created by katemorris on 28/11/2014.
 */
package manicminer.constant
{
    public class EmbeddedGraphics
    {
        [Embed(source="../../../resources/assets.swf", symbol="gfxBackground")]
        public static var gfxBackground : Class;

        [Embed(source="../../../resources/assets.swf", symbol="gfxTimer")]
        public static var gfxTimer : Class;

        [Embed(source="../../../resources/assets.swf", symbol="gfxPlayButton")]
        public static var gfxPlayButton : Class;

        [Embed(source="../../../resources/assets.swf", symbol="gfxPlayAgainButton")]
        public static var gfxPlayAgainButton : Class;

        [Embed(source="../../../resources/assets.swf", symbol="gfxGemBlue")]
        public static var gfxGemBlue : Class;

        [Embed(source="../../../resources/assets.swf", symbol="gfxGemGreen")]
        public static var gfxGemGreen : Class;

        [Embed(source="../../../resources/assets.swf", symbol="gfxGemPurple")]
        public static var gfxGemPurple : Class;

        [Embed(source="../../../resources/assets.swf", symbol="gfxGemRed")]
        public static var gfxGemRed : Class;

        [Embed(source="../../../resources/assets.swf", symbol="gfxGemYellow")]
        public static var gfxGemYellow : Class;

        [Embed(source="../../../resources/assets.swf", symbol="gfxSelector")]
        public static var gfxSelector : Class;

        [Embed(source="../../../resources/assets.swf", symbol="gfxInstructionScreen")]
        public static var gfxInstructionScreen : Class;

        [Embed(source="../../../resources/assets.swf", symbol="gfxScoreScreen")]
        public static var gfxScoreScreen : Class;

        [Embed(source="../../../resources/assets.swf", symbol="gfxExplosion")]
        public static var gfxExplosion : Class;

        [Embed(source="../../../resources/RedStateBlueStateBB_reg.ttf",
                fontName="GameFont",
                fontFamily="GameFontFamily",
                mimeType="application/x-font-truetype",
                embedAsCFF="false")]
        public static var gameFont : Class;
    }
}
