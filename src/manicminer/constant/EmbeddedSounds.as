/**
 * Created by katemorris on 30/11/2014.
 */
package manicminer.constant
{
    public class EmbeddedSounds
    {
        [Embed(source="../../../resources/assets.swf", symbol="sfxMatch")]
        public static var sfxMatch : Class;

        [Embed(source="../../../resources/assets.swf", symbol="sfxMusic")]
        public static var sfxMusic : Class;

        [Embed(source="../../../resources/assets.swf", symbol="sfxSwap")]
        public static var sfxSwap : Class;

        [Embed(source="../../../resources/assets.swf", symbol="sfxTick")]
        public static var sfxTick : Class;

        [Embed(source="../../../resources/assets.swf", symbol="sfxWrong")]
        public static var sfxWrong : Class;

        [Embed(source="../../../resources/assets.swf", symbol="sfxClick")]
        public static var sfxClick : Class;

        [Embed(source="../../../resources/assets.swf", symbol="sfxOutro")]
        public static var sfxOutro : Class;
    }
}
