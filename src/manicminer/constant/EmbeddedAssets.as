/**
 * Created by katemorris on 28/11/2014.
 */
package manicminer.constant
{
    public class EmbeddedAssets
    {
        [Embed(source="../../../resources/assets.swf", symbol="lnkBackground")]
        public static var Background : Class;

        [Embed(source="../../../resources/assets.swf", symbol="lnkGemBlue")]
        public static var GemBlue : Class;

        [Embed(source="../../../resources/assets.swf", symbol="lnkGemGreen")]
        public static var GemGreen : Class;

        [Embed(source="../../../resources/assets.swf", symbol="lnkGemPurple")]
        public static var GemPurple : Class;

        [Embed(source="../../../resources/assets.swf", symbol="lnkGemRed")]
        public static var GemRed : Class;

        [Embed(source="../../../resources/assets.swf", symbol="lnkGemYellow")]
        public static var GemYellow : Class;

        [Embed(source="../../../resources/assets.swf", symbol="lnkSelector")]
        public static var Selector : Class;

        [Embed(source="../../../resources/assets.swf", symbol="lnkInstructionScreen")]
        public static var InstructionScreen : Class;

        [Embed(source="../../../resources/assets.swf", symbol="lnkScoreScreen")]
        public static var ScoreScreen : Class;
    }
}
