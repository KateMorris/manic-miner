/**
 * Created by katemorris on 28/11/2014.
 */
package manicminer.event {
import flash.events.Event;

import manicminer.controller.component.GemComponent;

public class GemSelectEvent extends Event
{
    public static var GEM_SELECT : String = "manicminer.events.GemSelectEvent::GEM_SELECT";

    private var _gem : GemComponent;

    public function GemSelectEvent(gem : GemComponent, bubbles : Boolean = false)
    {
        super(GEM_SELECT, bubbles);

        _gem = gem;
    }

    public function getGem() : GemComponent
    {
        return _gem;
    }
}
}
