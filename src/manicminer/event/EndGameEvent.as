/**
 * Created by katemorris on 28/11/2014.
 */
package manicminer.event {
import flash.events.Event;

public class EndGameEvent extends Event
{
    public static const END_GAME : String = "manicminer.event.EndGameEvent::END_GAME";
    private var _score : int;

    public function EndGameEvent(score : int)
    {
        _score = score;

        super(END_GAME);
    }

    public function getScore() : int
    {
        return _score;
    }
}
}
