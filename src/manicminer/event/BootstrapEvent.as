/**
 * Created by katemorris on 28/11/2014.
 */
package manicminer.event
{
import flash.events.Event;

public class BootstrapEvent extends Event
{
    public static const BOOTSTRAP : String = "manicminer.event.BootstrapEvent.BOOTSTRAP";
    public static const BOOTSTRAP_COMPLETE : String = "manicminer.event.BootstrapEvent.BOOTSTRAP_COMPLETE";

    public function BootstrapEvent(type : String, bubbles : Boolean = false, cancelable : Boolean = false)
    {
        super(type, bubbles, cancelable);
    }
}
}
