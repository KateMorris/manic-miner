/**
 * Created by katemorris on 28/11/2014.
 */
package manicminer.event
{
import flash.events.Event;

public class ScoreEvent extends Event
{
    public static var ADD_SCORE : String = "manicminer.events.ScoreEvent::ADD_SCORE";

    private var _score : int;

    public function ScoreEvent(type : String, score : int)
    {
        _score = score;

        super(type);
    }

    public function getScore() : int
    {
        return _score;
    }
}
}
