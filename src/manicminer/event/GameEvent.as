/**
 * Created by katemorris on 28/11/2014.
 */
package manicminer.event
{

import flash.events.Event;

public class GameEvent extends Event
{
    public static const START_GAME : String = "manicminer.event.GameEvent::START_GAME";
    public static const TIME_UP : String = "manicminer.event.GameEvent::TIME_UP";

    public function GameEvent(type : String, bubbles : Boolean = false, cancelable : Boolean = false)
    {
        super(type, bubbles, cancelable);
    }
}
}
