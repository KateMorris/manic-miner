/**
 * Created by katemorris on 30/11/2014.
 */
package manicminer.event {
import flash.events.Event;

import manicminer.model.GemMatchModel;

public class GemMatchEvent extends Event
{
    public static var GEM_MATCH : String = "manicminer.events.GemMatchEvent::GEM_MATCH";
    private var _gemMatchModel : GemMatchModel;
    private var _scoreMatches : Boolean;

    public function GemMatchEvent(gemMatchModel : GemMatchModel, scoreMatches : Boolean)
    {
        super(GEM_MATCH);

        _gemMatchModel = gemMatchModel;
        _scoreMatches = scoreMatches;
    }

    public function get gemMatchModel() : GemMatchModel
    {
        return _gemMatchModel;
    }

    public function get scoreMatches() : Boolean
    {
        return _scoreMatches;
    }
}
}
