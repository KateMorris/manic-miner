/**
 * Created by katemorris on 30/11/2014.
 */
package manicminer.model
{
import manicminer.controller.component.GemComponent;
import mx.collections.ArrayList;

public class GemMatchModel
{
    private var _firstGem : GemComponent;
    private var _secondGem : GemComponent;

    private var _firstGemMatchesHorizontal : ArrayList;
    private var _firstGemMatchesVertical : ArrayList;
    private var _secondGemMatchesHorizontal : ArrayList;
    private var _secondGemMatchesVertical : ArrayList;

    public function GemMatchModel(firstGem : GemComponent, secondGem : GemComponent,
                                    firstGemMatchesHorizontal : ArrayList, firstGemMatchesVertical : ArrayList,
                                    secondGemMatchesHorizontal : ArrayList, secondGemMatchesVertical : ArrayList)
    {
        _firstGem = firstGem;
        _secondGem = secondGem;
        _firstGemMatchesHorizontal = firstGemMatchesHorizontal;
        _firstGemMatchesVertical = firstGemMatchesVertical;
        _secondGemMatchesHorizontal = secondGemMatchesHorizontal;
        _secondGemMatchesVertical = secondGemMatchesVertical;
    }

    public function getFirstGem() : GemComponent
    {
        return _firstGem;
    }

    public function getSecondGem() : GemComponent
    {
        return _secondGem;
    }

    public function getFirstGemMatchesHorizontal() : ArrayList
    {
        return _firstGemMatchesHorizontal;
    }

    public function getFirstGemMatchesVertical() : ArrayList
    {
        return _firstGemMatchesVertical;
    }

    public function getSecondGemMatchesHorizontal() : ArrayList
    {
        return _secondGemMatchesHorizontal;
    }

    public function getSecondGemMatchesVertical() : ArrayList
    {
        return _secondGemMatchesVertical;
    }

    public function firstGemHasMatches() : Boolean
    {
        return _firstGemMatchesHorizontal.length > 1 || _firstGemMatchesVertical.length > 1;
    }

    public function secondGemHasMatches() : Boolean
    {
        return _secondGemMatchesHorizontal.length > 1 || _secondGemMatchesVertical.length > 1;
    }

    public function hasMatches() : Boolean
    {
        return firstGemHasMatches() || secondGemHasMatches();
    }

    // Two added for initial swapped gems
    public function totalNumMatches() : int
    {
        var totalMatches : int = _firstGemMatchesHorizontal.length + _firstGemMatchesVertical.length +
                _secondGemMatchesHorizontal.length + _secondGemMatchesVertical.length;

        if (firstGemHasMatches())
        {
            totalMatches++;
        }

        if (secondGemHasMatches())
        {
            totalMatches++;
        }

        return totalMatches;
    }
}
}
